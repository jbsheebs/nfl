import {NgModule} from '@angular/core'
import {RouterModule} from "@angular/router";
import {rootRouterConfig} from "./app.routes";
import {AppComponent} from "./app.component";
import {FormsModule} from "@angular/forms";
import {BrowserModule} from "@angular/platform-browser";
import {HttpModule} from "@angular/http";
import {GameDetailsComponent} from './about/about.component';
import {HomeComponent} from './home/home.component';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import {HeaderComponent} from './header/header.component';
import {GridComponent} from "./grid/grid.component";
import {NFLService} from "./services/nfl.service";
import {GridRowComponent} from "./grid/grid-row.component";
import {TeamHelperService} from "./services/team-helper.service";
import {nvD3} from 'ng2-nvd3'
import {NVComponent} from "./nv/nv.component";
import {ChartContainerComponent} from "./charts/chart.component";

@NgModule({
  declarations: [AppComponent,
    GameDetailsComponent,
    HomeComponent,
    HeaderComponent,
    GridComponent,
    GridRowComponent,
    nvD3, NVComponent,
    ChartContainerComponent
  ],
  imports     : [BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(rootRouterConfig)],
  providers   : [
    NFLService,
    TeamHelperService,
    {provide: LocationStrategy,
      useClass: HashLocationStrategy}
      ],
  bootstrap   : [AppComponent]
})
export class AppModule {

}
