import {Component} from '@angular/core'
import {Employee} from '../models/employee.model'

@Component({
  selector: 'home',
  styleUrls: ['./home.component.css'],
  templateUrl: './home.component.html'
})
export class HomeComponent {

  languages = ['English', 'Spanish', 'Other', 'Yo Mama'];
  model = new Employee('Jeremy', 'Szyba', true, 'w2', 'English');

  firstNameToUpperCase(value: string){
    this.model.firstName = value;
  }
}
