import {Component} from "@angular/core";

@Component({
    selector: 'app-header',
    template: `
        <div class="row my-header">
            <!--<nav class="col-md-8 col-md-offset-2">-->
                <!--<ul class="nav nav-pills">-->
                    <!--<li routerLinkActive="active"><a [routerLink]="['/messages']" href="">Messenger</a></li>-->
                    <!--<li routerLinkActive="active"><a [routerLink]="['/auth']" href="">Authentication</a></li>-->
                <!--</ul>-->
            <!--</nav>-->
        </div>
    `,
    styles: [`
      .my-header {
        position: fixed;
        top: 0;
        right: 0;
        left: 0;
        height: 50px;
        background-color: #656eff;
      }
      
    `]
})
export class HeaderComponent {

}
