import {Component, Input, OnInit} from '@angular/core';
import {Game} from "../models/game.model";
import {Router} from "@angular/router";
import {NFLService} from "../services/nfl.service";

@Component({
  selector: 'grid-row',
  templateUrl: 'grid-row.component.html'
})
export class GridRowComponent implements OnInit{

  constructor(private router: Router, private nflService: NFLService) {}

  @Input('inputGame') game: Game;
  path: string = '../nfl-teams/';
  homeFullPath = '';
  awayFullPath = '';

  ngOnInit(): void {
    this.homeFullPath = this.path + this.game.home.image;
    this.awayFullPath = this.path + this.game.visitor.image;
  }

  redirectToCharts() {
    console.log('redirecting to game details');
    this.nflService.setTeams(this.game.home, this.game.visitor);
    this.nflService.setActiveGame(this.game);
    this.router.navigate(['/nvd3']);
  }
}
