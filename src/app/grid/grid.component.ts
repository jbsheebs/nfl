import {Component, OnInit} from '@angular/core';

import {NFLService} from "../services/nfl.service";
import {Game} from "../models/game.model";
import {TeamHelperService} from "../services/team-helper.service";

@Component({
  selector: 'nfl-grid',
  templateUrl: './grid.component.html'
})
export class GridComponent implements OnInit {

  constructor(public nflService: NFLService,
              private teamHelper: TeamHelperService) {}

  nflGames:Game[] = [];

  years = this.teamHelper.years;
  weeks = this.teamHelper.weeks;

  activeWeek;
  activeYear;

  onChange(){
    this.getGames(this.activeYear, this.activeWeek.code);
  }

  ngOnInit() {
    this.activeWeek = this.weeks[0];
    this.activeYear = this.years[1];
    this.getGames(this.activeYear, this.activeWeek.code);
  }

  getGames(year: string, week: string){
    this.nflService.getGames(year, week)
      .subscribe((games) => {
        this.nflGames = games;
      });
  }
}
