import {Http, Response} from "@angular/http";
import {Injectable} from "@angular/core";
import 'rxjs/Rx';
import {Observable} from "rxjs";
import {Game} from "../models/game.model";
import {TeamHelperService} from "./team-helper.service";
import {Team} from "../models/team.model";
import {RushingModel} from "../models/rushing-zone.model";

@Injectable()
export class NFLService {

  private games: Game[] = [];
  private activeGame: Game;
  private homeTeam: Team;
  private awayTeam: Team;
  private totalRushingData = [];

  setTeams(homeTeam: Team, awayTeam: Team){
    this.homeTeam = homeTeam;
    this.awayTeam = awayTeam;
  }

  getAwayTeam(){
    return this.awayTeam;
  }

  getHomeTeam(){
    return this.homeTeam;
  }

  setActiveGame(game: Game){
    this.activeGame = game;
  }

  getActiveGame(){
    return this.activeGame;
  }

  constructor(private http: Http, private teamHelper: TeamHelperService) {}

  getGames(season: string, week: string) {
    // let url = `http://ec2-54-210-140-69.compute-1.amazonaws.com:5000/games/${season}/${week}`;
    let url = `http://localhost:5000/games/${season}/${week}`;
    return this.http.get(url)
      .map((response: Response) => {
        const games = response.json();
        let transformedGames: Game[] = [];
        for (let game of games) {
          let homeTeam = this.teamHelper.getTeamByNickname(game.home);
          let awayTeam = this.teamHelper.getTeamByNickname(game.visitor);
          transformedGames.push(new Game(awayTeam, homeTeam, game.temperature, game.weather, game.weekday, game.surface, game.stadium));
        }
        this.games = transformedGames;
        return transformedGames;
      })
      .catch((error: Response) => Observable.throw(error.json()));
  }

  getOffensiveRushingZoneDataByTeam(team: string): Observable<RushingModel> {
    let url = `http://localhost:5000/zone/rush/team/yr/off/2016/${team}`;
    return this.http.get(url)
      .map((response: Response) => {
        const rushingTotalsJSON = response.json();
        const rushingTotals = rushingTotalsJSON[0];
        return new RushingModel(
          rushingTotals.RT,
          rushingTotals.LG,
          rushingTotals.LE,
          rushingTotals.MD,
          rushingTotals.RE,
          rushingTotals.LT,
          rushingTotals.RG,
          rushingTotals.total,
          rushingTotals.year,
          rushingTotals.offensive_team,
          'offense'
        )
      })
      .catch((error: Response) => Observable.throw(error.json()));
  }

  getDefensiveRushingZoneDataByTeam(team: string) {
    let url = `http://localhost:5000/zone/rush/team/yr/def/2016/${team}`;
    return this.http.get(url)
      .map((response: Response) => {
        const rushingTotalsJSON = response.json();
        const rushingTotals = rushingTotalsJSON[0];
        return new RushingModel(
          rushingTotals.RT,
          rushingTotals.LG,
          rushingTotals.LE,
          rushingTotals.MD,
          rushingTotals.RE,
          rushingTotals.LT,
          rushingTotals.RG,
          rushingTotals.total,
          rushingTotals.year,
          rushingTotals.defensive_team,
          'defense'
        );
      })
      .catch((error: Response) => Observable.throw(error.json()));
  }
}
