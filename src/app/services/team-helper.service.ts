import {Injectable} from "@angular/core";

import {Team} from "../models/team.model";

@Injectable()
export class TeamHelperService {

  years = ['2016','2015','2014','2013','2012','2011','2010'];
  weeks = [
    {text: "One", code: "1"},
    {text: "Two", code: "2"},
    {text: "Three", code: "3"},
    {text: "Four", code: "4"},
    {text: "Five", code: "5"},
    {text: "Six", code: "6"},
    {text: "Seven", code: "7"},
    {text: "Eight", code: "8"},
    {text: "Nine", code: "9"},
    {text: "Ten", code: "10"},
    {text: "Eleven", code: "11"},
    {text: "Twelve", code: "12"},
    {text: "Thirteen", code: "13"},
    {text: "Fourteen", code: "14"},
    {text: "Fifteen", code: "15"},
    {text: "Sixteen", code: "16"},
    {text: "Seventeen", code: "17"},
    {text: "Wild Card", code: "18"},
    {text: "Divisional Playoffs", code: "19"},
    {text: "Conference Championship", code: "20"},
    {text: "Superbowl", code: "21"},
  ];

  Teams: Team[] = [
    new Team('NE', 'New England', 'patriots.gif'),
    new Team('MIA', 'Miami', 'dolphins.gif'),
    new Team('NYJ', 'New York', 'jets.gif'),
    new Team('BUF', 'Buffalo', 'bills.gif'),

    new Team('BAL', 'Baltimore', 'ravens.gif'),
    new Team('CIN', 'Cincinnati', 'bengals.gif'),
    new Team('CLE', 'Cleveland', 'browns.gif'),
    new Team('PIT', 'Pittsburgh', 'steelers.gif'),

    new Team('HOU', 'Houston', 'texans.gif'),
    new Team('IND', 'Indianapolis', 'colts.gif'),
    new Team('JAC', 'Jacksonville', 'jaguars.gif'),
    new Team('TEN', 'Tennessee', 'titans.gif'),

    new Team('DEN', 'Denver', 'broncos.gif'),
    new Team('KC', 'Kansas City', 'chiefs.gif'),
    new Team('OAK', 'Oakland', 'raiders.gif'),
    new Team('SD', 'San Diego', 'chargers.gif'),

    new Team('CHI', 'Chicago', 'bears.gif'),
    new Team('DET', 'Detroit', 'lions.gif'),
    new Team('GB', 'Green Bay', 'packers.gif'),
    new Team('MIN', 'Minnesota', 'vikings.gif'),

    new Team('ATL', 'Atlanta', 'falcons.gif'),
    new Team('CAR', 'Carolina', 'panthers.gif'),
    new Team('NO', 'New Orleans', 'saints.gif'),
    new Team('TB', 'Tampa Bay', 'buccaneers.gif'),

    new Team('ARI', 'Arizona', 'cardinals.gif'),
    new Team('STL', 'Los Angeles', 'rams.gif'),
    new Team('SF', 'San Francisco', '49ers.gif'),
    new Team('SEA', 'Seattle', 'seahawks.gif'),

    new Team('DAL', 'Dallas', 'cowboys.gif'),
    new Team('NYG', 'NeW York', 'giants.gif'),
    new Team('PHI', 'Philadelphia', 'eagles.gif'),
    new Team('WAS', 'Washington', 'redskins.gif'),
  ]

  getTeamByNickname(name: string){
    if (name == 'LA') {
      name = 'STL';
    }
    return this.Teams.filter(team => team.nickname == name)[0];
  }
}
