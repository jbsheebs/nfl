import {Routes} from '@angular/router';
import {GameDetailsComponent} from './about/about.component';
import {GridComponent} from "./grid/grid.component";
import {ChartContainerComponent} from "./charts/chart.component";

export const rootRouterConfig: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: GridComponent},
  {path: 'details', component: GameDetailsComponent},
  {path: 'nvd3', component: ChartContainerComponent}















  // {path: 'github', component: RepoBrowserComponent,
  //   children: [
  //     {path: '', component: RepoListComponent},
  //     {path: ':org', component: RepoListComponent,
  //       children: [
  //         {path: '', component: RepoDetailComponent},
  //         {path: ':repo', component: RepoDetailComponent}
  //       ]
  //     }]
  // }
];

