import {Component, OnInit, Input, AfterViewInit} from '@angular/core';
import {NFLService} from "../services/nfl.service";
import {RushingModel} from "../models/rushing-zone.model";
import {Observable} from "rxjs";
import {Team} from "../models/team.model";
declare let d3: any;

@Component({
  selector: 'offensive-rush',
  template: `<div><nvd3 [options]="options" [data]="data"></nvd3></div>`
})
export class NVComponent implements OnInit {

  options;
  data;
  minY: number;
  @Input() offensiveTeam: Team;
  @Input() defensiveTeam: Team;

  constructor(public nflService: NFLService) {}

  getOffensiveRushingData(team: string){
    return this.nflService.getOffensiveRushingZoneDataByTeam(team);
  }

  getDefensiveRushingData(team: string){
    return this.nflService.getDefensiveRushingZoneDataByTeam(team);
  }

  ngOnInit(){
    Observable.forkJoin(
      this.getOffensiveRushingData(this.offensiveTeam.nickname),
      this.getDefensiveRushingData(this.defensiveTeam.nickname)
    ).subscribe(
      data => {
        let offensiveData = data[0];
        let defensiveData = data[1];
        this.offensiveTeam.setOffensiveRushingModel(offensiveData);
        this.defensiveTeam.setDefensiveRushingModel(defensiveData);
        let tempData = [
          offensiveData.transformDataForChart()[0],
          defensiveData.getNegativeValues().transformDataForChart()[0]
        ];
        this.minY = defensiveData.getNegativeValues().getMinDefensiveValue();
        this.options = {
          chart: {
            forceY: this.minY,
            type: 'multiBarHorizontalChart',
            height: 400,
            margin: {
              top: 50,
              right: 20,
              bottom: 50,
              left: 100
            },
            x: function (d) {
              return d.x;
            },
            y: function (d) {
              return d.y;
            },
            showValues: true,
            valueFormat: function (d) {
              return d3.format(',.0f')(d);
            },
            duration: 500,
            xAxis: {
              axisLabel: 'Rushing Zone',
              margin: {
                left: 100
              }
            },
            yAxis: {
              axisLabel: 'Total Yards',
              axisLabelDistance: -10
            }
          }
        };
        this.data = tempData;
        console.log(this.minY);
      }
    );
  }
}
