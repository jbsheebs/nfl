
import {Team} from "./team.model";

export class Game {
    constructor(
        public visitor: Team,
        public home: Team,
        public temperature: string,
        public weather: string,
        public weekday: string,
        public surface: string,
        public stadium: string
    ) {}

}
