export class RushingModel {

  chartData;

  constructor(
    public leftEnd: string,
    public leftTackle: string,
    public leftGuard: string,
    public middle: string,
    public rightGuard: string,
    public rightTackle: string,
    public rightEnd: string,
    public total: string,
    public year: string,
    public teamName: string,
    public offenseOrDefense: string
  ) {}

   keys = ['Left End', 'Left Tackle', 'Left Guard', 'Middle', 'Right Guard', 'Right Tackle', 'Right End'];

  getNegativeValues(): RushingModel{
    return new RushingModel(
      '-' + this.leftEnd,
      '-' + this.leftTackle,
      '-' + this.leftGuard,
      '-' + this.middle,
      '-' + this.rightGuard,
      '-' + this.rightTackle,
      '-' + this.rightEnd,
      '-' + this.total,
      this.year,
      this.teamName,
      this.offenseOrDefense
    );
  }

  getMinDefensiveValue(): number {
    let vals = Object.values(this).slice(0, 7).sort(function (a, b) {
      let aNum = +a;
      let bNum = +b;
      return aNum - bNum;
    });
    return +vals[0];
  }

  transformDataForChart(): any[]{
    let vals = Object.values(this);
    let mergedData = [];
    // this works because the keys are in the same order as the values
    this.keys.map(function(e, i) {
        mergedData.push({x: e, y: vals[i]});
    });
    return [
      {
        values: mergedData,
        key: `${this.teamName} ${this.offenseOrDefense}`
      }
    ];
  }
}
