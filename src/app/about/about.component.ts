import {Component, OnInit} from '@angular/core';

import {NFLService} from "../services/nfl.service";
import {Game} from "../models/game.model";

@Component({
  selector: 'about',
  styleUrls: ['./about.component.css'],
  template: `
    <h3>Game Details</h3>
    <div *ngFor="let item of gameKeysAndValues">
      <span>{{item.key}}</span>
      <span>{{item.value}}</span>
    </div>
  `
})
export class GameDetailsComponent implements OnInit {

  constructor(private nflService: NFLService) {}
  private activeGame: Game;
  private gameKeysAndValues = [];

  ngOnInit(): void {
    this.activeGame = this.nflService.getActiveGame();
    Object.entries(this.activeGame).forEach(
      ([key, value]) => this.gameKeysAndValues.push({key: key, value: value})
    );
  }
}
