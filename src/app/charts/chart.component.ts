import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {NFLService} from "../services/nfl.service";
import {Team} from "../models/team.model";

@Component({
  selector: 'nvd3-chart',
  templateUrl: 'chart.component.html'
})
export class ChartContainerComponent implements OnInit {

  homeTeam: Team;
  awayTeam: Team;

  ngOnInit() {
    this.homeTeam = this.nflService.getHomeTeam();
    this.awayTeam = this.nflService.getAwayTeam();
  }

  constructor(private route: ActivatedRoute,
              private nflService: NFLService) {}

}

// just a way to get data attached to a route

// this.route.data.subscribe(data => {
//     this.homeTeam = data.homeTeam;
//     this.awayTeam = data.awayTeam;
//     console.log(data);
//   });
